# DESSERT SYSTEM 

Sistema para el control de ventas de una heladería

## EJECUCIÓN

Para ejecutar el sistema debe tener instalado previamente 

* `Python3`
* Modulo ImgTK : `$ sudo apt-get install python3-pil python3-pil.imagetk`

Se debe hacer desde una terminal de la siguiente forma:

#### `$ cd paradigmas-project/`

#### `$ ./test.py`

## USO

El sistema cuenta con varios menús, se podrá navegar de forma gráfica.

* Menú Principal
  * Menú Productos
    * Vender Productos
    * Devolver Productos
  * Menú Clientes
  * Ver total en caja
  * Ver cierres anteriores
  * Cierre de caja

## CONTENIDO

En la carpeta paradigmas-project se encuentran los siguientes archivos y carpetas:

* diagrama:  
  *  diagrama_paradigmas.png : Diagrama de clases del sistema
  *  packages.png : Diagrama de los paquetes del sistema
* hmtl: archivos html generados con la librería de Python `epydoc`para facilitar la lectura de la documentación del sistema
* clases.py
* clientes.pickle
* controlador.py
* excepciones.py
* modelo.py
* ordenes.pickle
* test.py
* vista_tk.py

### GitLab

El código se encuentra disponible para ser descargado desde GitLab:

[Link](https://gitlab.com/isaux13/paradigmas-project-final)

