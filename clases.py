# __author__ = "Ana Isabel Jerez Uzcategui"
"""Sistema para el control de ventas de una heladeria"""

from abc import ABCMeta, abstractmethod
from datetime import datetime, date

# se asigna un precio al helado determinado al
# tipo de envase en el que se va a vender
precio_heladoporenvase = {
    "Cucurucho": 2500,
    "Vaso": 3500,
    "Isopor": 15000,
    "Balde": 60000
}


# abstract
class Empresa(metaclass=ABCMeta):
    """Clase abstracta de Empresa"""

    @abstractmethod
    def __init__(self, nombre, ruc, direccion, telefono):
        """
        Inicializador de la clase Empresa
            @params:
                nombre: nombre de la empresa
                ruc: ruc de la empresa
                direccion: direccion de la empresa
                telefono: telefono de la empresa
        """

        self.__nombre = nombre
        self.__ruc = ruc
        self.__direccion = direccion
        self.__telefono = telefono

    def __str__(self):

        return self.__nombre + '\n' + \
               "Ruc: " + self.__ruc + '\n' + \
               "Dir.: " + self.__direccion + '\n' + \
               "Telf.: " + self.__telefono


class Heladeria(Empresa):
    """Clase de la Heladeria"""

    total_caja = 0

    def __init__(self, productos, clientes, *args):
        """Inicializador de la clase Heladeria
            @params:
                productos: diccionario de la forma
                            ('Tipo producto', lista_productos)
                clientes: lista de los clientes registrados
        """

        super().__init__(*args)
        self.__clientes = clientes
        self.__productos = productos

    def registrar_cliente(self, cliente):
        """Metodo para registrar un nuevo cliente
            @params: 
                cliente: cliente que se registrará
        """

        self.__clientes.append(cliente)

    def modificar_cliente(self, index, nombre, apellido):
        """Método para modificar un cliente
            @params:
                index: indice del cliente en la lista
                nombre: nombre a modificar
                apellido: apellido a modificar
        """

        self.__clientes[index].nombre = nombre.upper()
        self.__clientes[index].apellido = apellido.upper()

    def eliminar_cliente(self, cliente):
        """Metodo para eliminar un cliente
            @params:
                cliente: cliente que será eliminado
        """

        self.__clientes.remove(cliente)

    @property
    def clientes(self):
        """Metodo que retorna la lista de clientes"""

        return self.__clientes

    @property
    def productos(self):
        """Metodo que retorna los productos"""

        return self.__productos

    @staticmethod
    def total():
        """Metodo estatico para consultar el total en caja"""

        return Heladeria.total_caja

    @staticmethod
    def cargar_caja(monto):
        """Metodo para aumentar el capital caja
            @params:
                monto: el capital que se va a cargar
        """

        Heladeria.total_caja += monto

    @staticmethod
    def reintegrar(monto):
        """"Metodo para reintegrar dinero desde la caja
            @params:
                monto: capital que se reintegra desde la caja
        """

        Heladeria.total_caja -= monto


# abstract
class Persona(metaclass=ABCMeta):
    """Clase asbtracta de Persona"""

    def __init__(self, ci, nombre, apellido):
        """
        Inicializador de la clase Persona
            @params:
                ci: cedula del cliente
                nombre: nombre del cliente
                apellido: apellido del cliente
        """

        self.__ci = int(ci)
        self.__nombre = nombre.upper()
        self.__apellido = apellido.upper()

    @property
    def ci(self):
        """Metodo para obtener la cedula del cliente"""

        return self.__ci
    
    @property
    def nombre(self):
        """Metodo para obtener el nombre del cliente"""

        return self.__nombre

    @property
    def apellido(self):
        """Metodo para obtener el apellido del cliente"""
        
        return self.__apellido

    @nombre.setter
    def nombre(self, nombre):
        self.__nombre = nombre

    @apellido.setter
    def apellido(self, apellido):
        self.__apellido = apellido

    def __str__(self):
        return  'Cedula:   ' + str(self.__ci) + '\n' + \
                'Nombre:   ' + self.__nombre + '\n' + \
                'Apellido: ' + self.__apellido

class Cliente(Persona):
    """Clase del Cliente"""

    def __init__(self, *args):
        """Inicializador de la clase Cliente"""

        super().__init__(*args)


# abstract
class Producto(metaclass=ABCMeta):
    """Clase abstracta del Producto"""

    def __init__(self, descripcion, precio):
        """
        Inicializador de la clase Producto
            @params:
                descripcion: nombre del producto
                precio: precio del producto
        """

        self.__descripcion = descripcion
        self.__precio = precio

    @property
    def descripcion(self):
        """Metodo para obtener la descripcion del producto"""

        return self.__descripcion

    @property
    def precio(self):
        """Metodo para obtener el precio del producto"""

        return self.__precio

    def vender(self):
        """Metodo para vender un producto"""

        Heladeria.cargar_caja(self.__precio)     # se carga en caja el monto


    def __str__(self):
 
        return '{:<20}'.format(self.__descripcion) + '{:>9}'.format(self.__precio)


# abstract
class Helado(Producto, metaclass=ABCMeta):
    """Clase abstracta Helado"""

    descripcion_helado = "Helado"

    def __init__(self, precio, sabor, envase):
        """
        Inicializacion de la clase Helado
            @params:
                precio: precio del producto
                sabor: sabor del helado
                envase: envase de referencia 
        """

        super().__init__(Helado.descripcion_helado + ' ' + sabor[:5] + ' en ' + envase[:4], precio)
        self.__sabor = sabor


class HeladoChocolate(Helado):
    """Clase HeladoChocolate"""
    
    descripcion = "Chocolate"

    def __init__(self, envase):
        """Inicializacion de la clase HeladoChocolate"""

        precio = precio_heladoporenvase[envase.descripcion]
        super().__init__(precio, HeladoChocolate.descripcion, envase.descripcion)
        self.__envase = envase
        

class HeladoAmericana(Helado):
    """Clase HeladoAmericana"""

    descripcion = "Americana"

    def __init__(self, envase):
        """Inicializacion de la clase HeladoAmericana"""

        precio = precio_heladoporenvase[envase.descripcion]
        super().__init__(precio, HeladoAmericana.descripcion, envase.descripcion)
        self.__envase = envase


class HeladoVainilla(Helado):
    """Clase HeladoVainilla"""

    descripcion = "Vainilla"

    def __init__(self, envase):
        """Inicializacion de la clase HeladoVainilla"""

        precio = precio_heladoporenvase[envase.descripcion]
        super().__init__(precio, HeladoVainilla.descripcion, envase.descripcion)


class HeladoLimon(Helado):
    """Clase HeladoLimon"""

    descripcion = "Limon"

    def __init__(self, envase):
        """Inicializacion de la clase HeladoLimon"""
        
        precio = precio_heladoporenvase[envase.descripcion]
        super().__init__(precio, HeladoLimon.descripcion, envase.descripcion)
        self.__envase = envase


# abstract
class Envase(Producto, metaclass=ABCMeta):
    """Clase abstracta de Envase"""

    descripcion_envase = "Envase"

    def __init__(self, descripcion, precio):
        """Inicializacion de la clase Envase"""

        super().__init__(str(Envase.descripcion_envase + ' ' + descripcion), precio)


class Cucurucho(Envase):
    """Clase Cucurucho"""
    
    descripcion = 'Cucurucho'
    precio = 1000

    def __init__(self):
        """Inicializacion de la clase Cucurucho"""

        super().__init__(Cucurucho.descripcion, Cucurucho.precio)


class Vaso(Envase):
    """Clase Vaso"""

    descripcion = 'Vaso'
    precio = 200

    def __init__(self):
        """Inicializacion de la clase Vaso"""

        super().__init__(Vaso.descripcion, Vaso.precio) 


class Isopor(Envase):
    """Clase Isopor"""

    descripcion = 'Isopor'
    precio = 2500

    def __init__(self):
        """Inicializacion de la clase Isopor"""
        super().__init__(Isopor.descripcion, Isopor.precio)


# abstract
class Devolvible(metaclass=ABCMeta):
    """Clase abstracta Devolvible"""

    def devolver(self):
        Heladeria.reintegrar(self.precio)


class Balde(Envase, Devolvible):
    """Clase Balde"""

    descripcion = 'Balde'
    precio = 12000

    def __init__(self):
        """Inicializacion de la clase Balde"""

        super().__init__(Balde.descripcion, Balde.precio)


class Torta(Producto):
    """Clase Torta"""

    descripcion = 'Porcion Torta'
    precio = 5000

    def __init__(self):
        """Inicializacion de la clase Torta"""

        super().__init__(Torta.descripcion, Torta.precio)


# abstract
class Bebida(Producto, metaclass=ABCMeta):
    """Clase abstracta de Bebida"""

    def __init__(self, descripcion, precio):
        """Inicilizacion de la clase Bebida"""

        super().__init__(descripcion, precio)


class Jugo(Bebida):
    """Clase de Jugo"""

    descripcion = 'Jugo'
    precio = 2500

    def __init__(self):
        """Inicializacion de la clase Jugo"""

        super().__init__(Jugo.descripcion, Jugo.precio)


class Gaseosa(Bebida, Devolvible):
    """Clase de Gaseosa"""

    descripcion = 'Gaseosa'
    precio = 3500

    def __init__(self):
        """Inicializacion de la clase Gaseosa"""
        super().__init__(Gaseosa.descripcion, Gaseosa.precio)


class Orden:
    """Clase Orden"""

    numero_orden = 0
    
    def __init__(self):
        """Inicializador de la clase Orden"""

        Orden.numero_orden += 1
        date = datetime.now()
        self.__productos = []
        self.__totalorden = 0
        self.__numero = Orden.numero_orden
        self.__fecha = date.strftime("%d-%m-%Y")
        self.__hora = date.strftime("%H:%M")

    @property
    def productos(self):
        """Metodo para obtener una lista de los productos"""

        return self.__productos

    @property
    def totalorden(self):
        """Metodo para obtener el total de la orden"""

        return self.__totalorden

    def cargar_producto(self, producto):
        """Metodo para cargar un producto en la orden"""

        self.__productos.append(producto)
        self.__totalorden += producto.precio
        
    def eliminar_producto(self, producto):
        """Metodo para eliminar un producto de la orden"""

        self.__productos.remove(producto)
        self.__totalorden -= producto.precio

    def __str__(self):
        
        hora = self.__hora
        numero = '{:0>5}'.format(str(self.__numero))
        productos = ''
        for i in self.__productos:
            productos += i.__str__() + '\n'
        total = str(self.__totalorden)
        delimitador_str = "------------------------------------"

        return delimitador_str + '\n' +\
                'Hora: ' + hora + '\tNro: ' + numero + '\n' +\
                delimitador_str + '\n' +\
                productos + '\n' + \
                delimitador_str + '\n' + \
                'Total: ' + total + '\n' + \
                delimitador_str


class Comprobante:
    """Clase Comprobante"""
    
    numero_comprobante = 0
    
    def __init__(self, orden, cliente, heladeria):
        """Inicializacion de la clase Comprobante"""

        date = datetime.now()
        self.__productos = orden.productos
        self.__fecha = date.strftime("%d-%m-%Y")
        self.__hora = date.strftime("%H:%M")
        self.__total = orden.totalorden
        self.__cliente = cliente.__str__()
        self.__heladeria = heladeria.__str__()
        Comprobante.numero_comprobante += 1

    def __str__(self):

        fecha = 'Fecha: ' + self.__fecha + '\n' + 'Hora: ' + self.__hora
        cliente_str = self.__cliente
        heladeria_str = self.__heladeria
        productos_str = ''
        delimitador_str = '-------------------------------------------'
        descripcion = "COMPROBANTE DE VENTA"
        numero_comprobante = '{:0>5}'.format(str(Comprobante.numero_comprobante))
        total = '{:20}'.format('Total:') + '{:9}'.format(self.__total)
        for i in self.__productos:
            productos_str += i.__str__() + '\n'
        return delimitador_str + '\n' + \
               descripcion+ '\t\tNro: ' + numero_comprobante + '\n' + \
               delimitador_str + '\n' + \
               heladeria_str + '\n' + \
               delimitador_str + '\n' + \
               fecha + '\n' + \
               cliente_str + '\n' + \
               delimitador_str + '\n' + \
               productos_str + \
               delimitador_str + '\n' + \
               total + '\n' + \
               delimitador_str + '\n'