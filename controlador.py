# __author__ = "Ana Isabel Jerez Uzcategui"
"""Sistema para el control de ventas de una heladeria"""

from abc import *

from clases import *
from modelo import *
from excepciones import *

class Controlador:

    heladeria = None 
    orden = None
    lista_orden_dia = []

    @staticmethod
    def iniciar():
        """Método para inicializar la heladeria"""
   
        try:
            lista_clientes = Modelo.obtener_clientes()
            productos = {'Helados': [HeladoChocolate, HeladoVainilla, HeladoAmericana, HeladoLimon],
                        'Envases': [Cucurucho, Vaso, Isopor, Balde],
                        'Bebidas': [Gaseosa, Jugo],
                        'Tortas': [Torta]}
            Controlador.heladeria = Heladeria(productos, lista_clientes, 'Heladería Arcobom', '12345-1', 'Ypacaraí', '123 123 123')
        except TypeError:
            return 'Ocurrió un error'
        
    # --------- Metodos relacionados a los clientes ----------
    @staticmethod
    def agregar_cliente(ci, nombre, apellido):
        """Método para agregar un cliente
            @parametros:
                ci: cedula del cliente
                nombre: Nombre del cliente
                apellido: apellido del cliente"""
        try:   
            # si no eiste, se crea el cliente
            if Controlador.heladeria == None:
                    raise SinHeladeriaError
            # se verifica si la cedula la existe en el registro
            ci = str(ci)
            while ci[0] == 0:
                ci.pop(0)
            ci = int(ci)
            cliente = Controlador.existe_cliente(ci)
            if cliente == None:
                if len(str(ci)) > 8:
                    raise CedulaMuyGrandeError
                if ci < 1:
                    raise NumeroNegativoError
                if type(ci) == float:
                    raise NumeroFlotanteError
                if not nombre or not apellido:
                    raise SinTextoError
                cliente = Cliente(ci, nombre, apellido)  
                Controlador.heladeria.registrar_cliente(cliente)
                Modelo.guardar_clientes(Controlador.heladeria.clientes)
                return cliente
            #si existe, se retorna un mensaje
            else:
                return 'Ya existe el cliente' + '\n' + cliente.__str__()
        except ValueError:
            return 'Cedula debe ser numerico' 
        except CedulaMuyGrandeError:
            return 'No se admite cedula con mas de 8 digitos'
        except NumeroNegativoError:
            return 'No se admiten numeros negativos'
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'
        except NumeroFlotanteError:
            return 'Cedula debe ser un numero entero'
        except SinTextoError:
            return 'Nombre y Apellido no pueden estar vacios'

    @staticmethod
    def existe_cliente(ci):
        """Metodo para verificar si un cliente existe en el registro
            @parametros:
                ci: cedula del cliente a buscar"""
        try:
            ci = int(ci)
            if Controlador.heladeria == None:
                raise SinHeladeriaError    
            # si cliente existe, retorna dicho cliente, si no, retorna None
            return next((cliente for cliente in Controlador.heladeria.clientes if cliente.ci == ci), None)
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'
        except ValueError:
            return 'Cedula debe ser numerico'

    @staticmethod
    def lista_clientes():
        """Metodo para obtener la lista de clientes"""
        
        try:
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            if Controlador.heladeria.clientes:
                return Controlador.heladeria.clientes
            else:
                return ['NO HAY CLIENTES']
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'
            
    @staticmethod
    def modificar_cliente(ci, nombre, apellido):
        """Metodo para modificar un cliente del registro
             @parametros:
                ci: cedula del cliente
                nombre: Nombre del cliente
                apellido: apellido del cliente
        """
        
        try:
            ci = int(ci)
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            # se verifica  que exista el cliente que se va a modificar, dado su ci
            cliente = Controlador.existe_cliente(ci)
            if cliente == None:
                return '\nEl usuario no existe\n'
            else:
                if not nombre or not apellido:
                    raise SinTextoError
                index = Controlador.heladeria.clientes.index(cliente)
                Controlador.heladeria.modificar_cliente(index, nombre, apellido)
                Modelo.guardar_clientes(Controlador.heladeria.clientes)
                return '\nSe modificó el cliente\n' + cliente.__str__()
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'
        except SinTextoError:
            return 'El Nombre y Apellido no pueden estar vacíos'
        except ValueError:
            return 'Cedula debe ser numerico'

    @staticmethod
    def eliminar_cliente(ci):
        """Metodo para eliminar un cliente del registro
             @parametros:
                ci: cedula del cliente
        """

        # se verifica  que exista el cliente que se va a eliminar, dado su ci
        try:
            ci = int(ci)
            cliente = Controlador.existe_cliente(ci)
            if cliente == None:
                return 'El usuario no existe'
            else:
                if Controlador.heladeria == None:
                    raise SinHeladeriaError
                Controlador.heladeria.eliminar_cliente(cliente)
                Modelo.guardar_clientes(Controlador.heladeria.clientes)
                return cliente
        except TypeError:
            return 'Se produjo un error'
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'
        except ValueError:
            return 'Cedula debe ser numerico'

    # --------- Metodos relacionados a las ventas ----------
    @staticmethod
    def crear_orden():
        """Metodo para crear una nueva orden de venta"""
        
        Controlador.orden = Orden()

    @staticmethod
    def ver_producto_orden():
        """Metodo para obtener la lista de productos en la orden actual"""

        try:
            if Controlador.orden == None:
                raise SinOrdenError
            return Controlador.orden.productos
        except SinOrdenError:
            return 'No se ha creado la orden'   

    @staticmethod
    def obtener_comprobante(cliente=None):
        """
        Metodo para obtener el comprobante actual de venta
            @params:
                cliente: cuando requiera comprobante con nombre
        """
        
        try:
            if Controlador.orden == None:
                raise SinOrdenError
            if not Controlador.orden.productos:
                raise OrdenVaciaError
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            # si cliente es None, se carga el cliente por defecto al comprobante
            if cliente == None:
                cliente = Cliente(0, 'XXXXX', 'XXXXX')
            # de lo contrario se carga el cliente ingresado
            comprobante = Comprobante(Controlador.orden, cliente, Controlador.heladeria)
            Controlador.lista_orden_dia.append(Controlador.orden)
            for i in Controlador.orden.productos:
                i.vender()
            Controlador.orden = None
            return comprobante
        except SinOrdenError:
            return 'No se creo la orden'
        except OrdenVaciaError:
            return 'No se cargaron productos'
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'

    @staticmethod
    def total_caja():
        """Metodo que devuelve el total del día en caja"""
        
        try:
            if Controlador.heladeria == None:
                raise SinOrdenError
            return Controlador.heladeria.total()
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'

    @staticmethod
    def devolver_producto(clase):
        """
        Metodo para devolver un producto
            @params:
                clase: la clase del producto que se devolverá
        """
        
        try:
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            if not isinstance(clase(), Devolvible):
                raise NoDevolvibleError
            producto = clase()
            if producto.precio > Controlador.heladeria.total():
                return 'La caja no cuenta con el capital suficiente'
            else:
                producto.devolver()
                return 'Monto reintegrado: Gs. ' + str(producto.precio)
        except NoDevolvibleError:
            return 'Este articulo no se puede devolver'
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'

    @staticmethod
    def agregar_producto_orden(clase, helado=False, *envase):
        """
        Metodo para realizar la venta de un producto
            @params:
                clase: nombre de la clase del producto 
                helado: si el producto es un helado será True
                *envase: parametro adicional si el producto es helado
            """

        producto = None
        try:
            if Controlador.orden == None:
                raise SinOrdenError
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            if helado:
                producto = clase(*envase)
            else:
                producto = clase() 
            Controlador.orden.cargar_producto(producto)
            return '\nSe cargó el producto' + '\n' + 'Total: Gs.' + str(Controlador.orden.totalorden) + '\n'
        except TypeError:
            return '\nEl parametro debe ser el nombre de la clase'
        except SinOrdenError:
            return '\nNo se ha creado una orden'
        except SinHeladeriaError:
            return '\nNo se ha instanciado la heladeria'

    @staticmethod
    def cancelar_orden():
        """Metodo encargado de anular la orden actual"""

        try:
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            if Controlador.orden == None:
                raise SinOrdenError
            Heladeria.reintegrar(Controlador.orden.totalorden)
            Controlador.orden = None
            return 'Orden candelada'
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'
        except SinOrdenError:
            return 'No se ha creado una orden'

    @staticmethod
    def total_orden():
        """Metodo para obtener el total de la orden actual"""

        try:
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            if Controlador.orden == None:
                raise SinOrdenError
            return Controlador.orden.totalorden
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'
        except SinOrdenError:
            return 'No se ha creado la orden'

    @staticmethod
    def eliminar_articulo_orden(index):
        """Metodo para eliminar el articulo de la orden actual"""

        try:
            if Controlador.orden == None:
                raise SinOrdenError
            producto = Controlador.orden.productos[index]
            Controlador.orden.eliminar_producto(producto)
            return 'Producto eliminado'
        except IndexError:
            return 'No se encuentra el articulo'
        except SinOrdenError:
            return 'No se ha creado la orden'

    # --------- Metodos relacionados a los productos ----------
    @staticmethod
    def productos_devolvibles():
        """Metodo que retorna las clases de los productos que se pueden devolver"""
        
        return [Balde, Gaseosa]

    @staticmethod
    def lista_bebidas():
        """Metodo para obtener la lista de bebidas"""
        
        try:
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            return Controlador.heladeria.productos['Bebidas']
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'

    @staticmethod
    def lista_helados():
        """Metodo para obtener la lista de helados"""

        try:
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            helados = []
            for i in Controlador.heladeria.productos['Envases']:
                for j in Controlador.heladeria.productos['Helados']:
                    helados.append((i,j))
            return helados
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'

    def lista_tortas():
        """Metodo para obtener la lista de tortas"""
        
        try:
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            return Controlador.heladeria.productos['Tortas']
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'

    def lista_envases():
        """Metodo para obtener la lista de envases"""
        
        try:
            if Controlador.heladeria == None:
                raise SinHeladeriaError
            return Controlador.heladeria.productos['Envases']
        except SinHeladeriaError:
            return 'No se ha instanciado la heladeria'

    @staticmethod
    def hacer_cierre(dia):
        """
        Metodo para realizar el cierre de caja
            @params:
                dia: el dia que se hace el cierre, 
                el cual servira para consulta posterior
        """

        try:
            if not Controlador.lista_orden_dia:
                raise SinOrdenesDiaError
            ordenes = Modelo.obtener_ordenes()
            if dia in ordenes:
                ordenes[dia] = ordenes[dia] + Controlador.lista_orden_dia
            else:
                ordenes[dia] = Controlador.lista_orden_dia
            Modelo.guardar_ordenes(ordenes)
            return Controlador.lista_orden_dia
        except SinOrdenesDiaError:
            return ['Sin ordenes']
        
    @staticmethod
    def ver_ordenes_dia(fecha):
        """Metodo para guardar en una variable las ordenes de un dia determinado"""

        try:
            datetime.strptime(fecha, '%d-%m-%Y')

            ordenes = Modelo.obtener_ordenes()
            if fecha in ordenes:
                return ordenes[fecha]
            else:
                return ['\nNo hay ordenes para el día: ' + fecha]
        except ValueError:
            return 'Fecha invalida'