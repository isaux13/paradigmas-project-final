% hecho: es_una_heladeria(X)
es_una_heladeria('Heladeria Arcobom').
%----PORDUCTOS--------
% hecho: es_una_bebida(X)
es_una_bebida(gaseosa).
es_una_bebida(jugo).
% hecho: es_un_envase(X)
es_un_envase(cucurucho).
es_un_envase(vaso).
es_un_envase(isopor).
es_un_envase(balde).
% hecho: es_un_sabor(X)
es_un_sabor(chocolate).
es_un_sabor(vainilla).
es_un_sabor(americana).
es_un_sabor(limon).
% hecho: precio_producto(X,Y)
precio_producto(cucurucho,1000).
precio_producto(vaso,200).
precio_producto(isopor,2500).
precio_producto(balde,12000).
precio_producto(gaseosa,3500).
precio_producto(jugo,2500).
precio_producto(helado-cucurucho,3500).
precio_producto(helado-vaso,2500).
precio_producto(helado-isopor,15000).
precio_producto(helado-balde,60000).
%-------------REGLAS------------------
envase(X):-es_un_envase(X).
bebida(X):-es_una_bebida(X).
sabor(X):-es_un_sabor(X).
helado(X,Y):-sabor(X),envase(Y).
producto(X):-envase(X);bebida(X).
producto(X,Y):-helado(X,Y).
vendible(X):-producto(X).
vendible(X,Y):-producto(X,Y).
devolvible(X):-producto(X);(X=gaseosa,X=balde).
%-------venta de varios productos de forma recursiva----
venderProducto(X,Y):-
    vender(X,0.0,Y).

vender([],Y,Y).

vender([Head|Tail],Y0,Y) :-
    precio_producto(Head,Cost),
    Y1 is Y0 + Cost,
    vender(Tail,Y1,Y).

%----------CLIENTES-------------
%----------HECHOS----------------
% hecho: es_cliente(X)
es_cliente(123456).
es_cliente(123450).
nombre_cliente(123456,'Ana').
nombre_cliente(123450,'Isabel').
apellido_cliente(123450,'Jerez').
apellido_cliente(123456,'Jerez').

%----------REGLAS----------------
cliente(X,Y,Z):-es_cliente(X),nombre_cliente(X,Y),apellido_cliente(X,Z).
persona(X,Y,Z):-cliente(X)


%%%
consultarClientes(X,Y,Z):-
    consultar(X).

consultar([],Y,Z).

consultar([Head|Tail]) :-
    cliente(Head,Y,Z)
    consultar(Tail).