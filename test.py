#!/usr/bin/python3
from vista_tk import *
from controlador import *
# __author__: "Ana Isabel Jerez Uzcategui"

"""SISTEMA DE CONTROL DE VENTAS DE UNA HELADERIA"""


class Aplicacion():
	"""Clase destinada a la ejecucion de la aplicacion"""
	@staticmethod
	def main():

		Controlador.iniciar()
		Vista.menu_principal()


if __name__ == "__main__":
    Aplicacion.main()    