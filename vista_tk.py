from tkinter import *
import tkinter.scrolledtext as tkst
from controlador import *
from tkinter import messagebox
import time
from PIL import ImageTk, Image
import time, locale


#---------------- Clases propias que heredan de los Widgets de Tkinter ------------
class Boton(Button):
	"""Clase Boton que herada de Button"""

	def __init__(self, texto, evento=None, parent=None, **config):   
		"""
			Inicializador de la Clase Boton
				@params: 
					texto: texto del botón
					evento: evento o comando que se ejecutará al hacer click
					parent: clase padre 
		"""

		Button.__init__(self, parent, text=texto, command=evento, font=("Arial Bold", 12), pady=4, **config)  
		self.configure(bg='white', fg='red', overrelief='flat', padx=2, pady=10, cursor='hand2')
		self.configure(activebackground='#fbe7ea', activeforeground='black', borderwidth=3)                                                   


class BotonAtrasCancelar(Boton):
	"""Clase BotonAtrasCancelar que herada de Boton"""

	def __init__(self, texto, evento=None,  parent=None, **config):
		"""
			Inicializador de la Clase BotonAtrasCancelar
				@params: 
					texto: texto del botón
					evento: evento o comando que se ejecutará al hacer click
					parent: clase padre 
		"""

		Boton.__init__(self, parent=parent, texto=texto, evento=evento, **config)
		self.configure(fg='black', bg='chocolate1') 

class Ventana(Tk):
	"""Clase Ventana que hereda de Tk"""

	def __init__(self, nombre):
		"""
			Inicializador de la Clase Ventana
				@params: 
					nombre: texto que aparecerá en el top bar de la ventana
		"""

		Tk.__init__(self)
		self.title(nombre)
		self.geometry("500x500")  
		self.pack_propagate(0)


class VentanaToplevel(Toplevel):
	"""Clase VentanaToplevel que hereda de TopLevel"""

	def __init__(self, parent=None, **options):
		"""
			Inicializador de la Clase VentanaToplevel
				@params: 
					parent: clase padre 
		"""

		Toplevel.__init__(self, parent, **options)
		self.configure(bg='#e51924', height=500, width=500)
		self.resizable(0,0)


class Titulo(Label):
	"""Clase Título que hereda de Label"""

	def __init__(self, texto, parent=None, **options):
		"""
			Inicializador de la Clase Titulo
				@params: 
					texto: texto del Titulo
					parent: clase padre 
		"""

		Label.__init__(self, parent, text=texto, **options)
		self.configure(font=("Arial Bold", 17), bg='#e51924', fg='white',  pady=10)


class Entrada(Entry):
	"""Clase Entrada que hereda de Entry"""

	def __init__(self, txt_var, parent=None, **options):
		"""
			Inicializador de la Clase Entrada
				@params: 
					text_var: variable donde se guardará el texto de la entrada
					parent: clase padre 
		"""

		Entry.__init__(self, parent, textvariable=txt_var, **options)
		self.configure(bg='white', fg='black')


class Paned(PanedWindow):
	"""Clase Paned que hereda de Paned Window"""

	def __init__(self, parent=None, **options):
		"""
			Inicializador de la Clase Paned
				@params: 
					parent: clase padre 
		"""

		PanedWindow.__init__(self, parent, borderwidth=0, **options)


class VentanaLblEnt(VentanaToplevel):
	"""
		Clase VentanaLblEnt que hereda de VentanaToplevel:
			Clase para crear una ventana a la cual se le agrega,
			consecutivamente, un par (label y entrada)

	"""

	def __init__(self, titulo, parent=None, **options):
		"""
			Inicializador de la clase VentanaLblEnt
				@params:
					titulo: titulo de la ventana
					parent: clase padre
		"""

		VentanaToplevel.__init__(self, parent, **options)
		label = Label(self, text=titulo, font=("Arial Bold", 20), pady=25, padx=10, bg='#e51924', fg='white')
		label.pack(side=TOP)

	def set_lbl_ent(self, txt_lbl, var_ent):
		"""Metodo que crea y agrega a la ventana un label y una entrada que le corresponde"""

		lbl = LblForEnt(txt_lbl, self)
		ent = Entrada(var_ent, self)
		lbl.pack(side=TOP)
		ent.pack(side=TOP)


class VentanaLblOpt(VentanaToplevel):
	"""
		Clase VentanaLblEnt que hereda de VentanaToplevel:
			Clase para crear una ventana a la cual se le agrega,
			consecutivamente, un par (label y OptionMenu)

	"""

	def __init__(self, titulo, parent=None, **options):
		"""
			Inicializador de la clase VentanaLblOpt
				@params:
					titulo: titulo de la ventana
					parent: clase padre
		"""

		VentanaToplevel.__init__(self, parent, **options)
		label = Label(self, text=titulo, font=("Arial Bold", 20), pady=25, padx=10, bg='#e51924', fg='white')
		label.pack(side=TOP)

	def set_lbl_opt(self, txt_lbl, var_opt, list_options):
		"""Metodo que crea y agrega un label y un MenuOption a la ventana"""

		lbl = LblForEnt(txt_lbl, self)
		opt = OptionMenu(self, var_opt, *list_options)
		opt.configure(bg='white', fg='black', relief=FLAT)
		lbl.pack(side=TOP)
		opt.pack(side=TOP)


class LblForEnt(Label):
	"""Clase LblForEnt que hereda de Label"""

	def __init__(self, texto, parent=None, **options):
		"""
			Inicializacion de la clase LblFotEnt
				@params:
					texto: texto que irá en el label
					@parent: clase padre

		"""

		Label.__init__(self, parent)
		self.config(text=texto, font=("Arial Bold", 12), pady=10, padx=20, bg='#e51924', fg='white')

class MenuBar(Menu):
	"""Clase Menubar que ehreda de Menu"""

	def __init__(self, parent=None, **options):
		Menu.__init__(self, parent, font=("Arial Bold", 13), bg='#e51924')
		self.configure(activebackground='white', activeforeground='black')


class ListaCaja(Listbox):
	"""Clase ListaCaja que hereda de la clase Listbox"""

	def __init__(self, parent=None, **options):
		Listbox.__init__(self, parent, width=30, bg='white', fg='black', font=("Courier New", 15))
		self.configure(activestyle='dotbox', bd=0, cursor='hand2', relief=FLAT)
		self.configure(selectbackground='#fbe7ea', selectforeground='#e51924')


class Vista():

	@staticmethod
	def menu_principal():
		"""metodo para crear un menu principal"""

		def ask_cierre():
			"""Metodo que pregunta al usuario si esta seguro de realizar el cierre de caja"""

			msj = messagebox.askquestion ('Hacer cierre','¿Hacer cierre de caja?', icon = 'warning')
			if msj == 'yes':
				Vista.menu_cerrar(ventana_principal)
			else:
				return

		def crear_orden():
			"""Metodo que crea una orden de venta"""

			Controlador.crear_orden()
			Vista.menu_vender(ventana_principal)


		ventana_principal = Ventana('Dessert System')
		ventana_principal.configure(bg='white')
		path = 'logo.jpeg'
		img = ImageTk.PhotoImage(Image.open(path))
		panel = Label(ventana_principal, image = img)
		panel.pack(side = "top", expand = True)
		#se instancia el Menu
		menubar = MenuBar(ventana_principal, cursor='hand2')
		#objetos relativos a menu productos		
		productos_menu = MenuBar(menubar, tearoff=0)
		productos_menu.add_command(label="Vender prodcutos", command=crear_orden)
		productos_menu.add_command(label="Devolver productos", command=lambda:Vista.menu_devolver(ventana_principal))
		menubar.add_cascade(label="Productos", menu=productos_menu)
		#objetos relativos a menu clientes
		clientes_menu = MenuBar(menubar, tearoff=0)
		clientes_menu.add_command(label="Ver Clientes", command=lambda:Vista.menu_clientes(ventana_principal))
		menubar.add_cascade(label="Clientes", menu=clientes_menu)
		#objetos relativos a menu caja
		caja_menu = MenuBar(menubar, tearoff=0)
		caja_menu.add_command(label="Ver total en caja", command=lambda:Vista.menu_caja(ventana_principal))
		menubar.add_cascade(label="Caja", menu=caja_menu)
		#objetos relativos a menu cierres
		cierres_menu = MenuBar(menubar, tearoff=0)
		cierres_menu.add_command(label="Ver cierres anteriores", command=lambda:Vista.formulario_cierres_anteriores(ventana_principal))
		menubar.add_cascade(label="Cierres", menu=cierres_menu)
		ventana_principal.config(menu=menubar)
		#boton para realizar cierre de caja
		boton = BotonAtrasCancelar(texto='Realizar cierre de caja', evento=ask_cierre, parent=ventana_principal)
		boton.pack(side=BOTTOM, fill=X)
		ventana_principal.mainloop()


	@staticmethod
	def menu_vender(ventana):
		"""Metodo para la creacion del Menu Vender"""

		def select_index(event):
			"""
				Callback que agrega un boton Eliminar
				al hacer click sobre un elemento del ListBox
			"""
			try:
				paned1.add(boton4)
			except IndexError:
				print('Fuera de ListBox')

		def eliminar_articulo():
			"""
				Metodo invocado al hacer click en Eliminar:
				Elimina un elemento del listBox y de la Orden
			"""

			try:
				index_art = lista_productos.curselection()[0]
				lista_productos.delete(index_art)
				Controlador.eliminar_articulo_orden(index_art)
				string_total = 'Total: Gs. ' + str(Controlador.total_orden())
				total.configure(text=string_total)
				productos = Controlador.ver_producto_orden()
				if not productos:
					boton2.configure(state=DISABLED)
					lista_productos.insert(END, 'Sin articulos')
					lista_productos.configure(state=DISABLED)
				paned1.remove(boton4)

			except IndexError:
				print('Fuera de ListBox')

		def ask_cancelar():
			"""Metodo para preguntar al usuario si desea cancelar la orden"""

			msj = messagebox.askquestion ('Cancelar Orden','¿Desea cancelar la orden?', icon = 'warning')
			if msj == 'yes':
				Controlador.cancelar_orden()
				ventana_top.destroy()
			else:
				return

		def ask_imprimir():
			"""Metodo para preguntar al usuario si desea imprimir el comprobante"""

			msj = messagebox.askquestion ('Terminar venta','¿Desea finalizar la venta?', icon = 'warning')
			if msj == 'yes':
				Vista.confirmar_cliente(ventana_top, ventana)
			else:
				return

		ventana_top = VentanaToplevel(ventana)
		ventana_top.geometry("400x500+500+100")
		paned1 = Paned(ventana_top, orient=VERTICAL)
		paned2 = Paned(ventana_top, orient= VERTICAL)
		sub_paned1 = Paned(paned2)
		sub_paned2 = Paned(paned2)
		paned3 = Paned(ventana_top)
		boton1 = Boton(texto='AGREGAR', evento=lambda:Vista.menu_tipos_productos(ventana_top, ventana), parent=paned1)
		boton2 = Boton(texto='IMPRIMIR', evento=ask_imprimir, parent=paned3)
		boton3 = BotonAtrasCancelar(texto='CANCELAR', evento=ask_cancelar, parent=paned3)
		boton4 = BotonAtrasCancelar(texto='ELIMINAR', evento=eliminar_articulo, parent=paned1)
		titulo = Titulo('Lista de Articulos', paned1)
		y_scroll = Scrollbar(sub_paned1, orient=VERTICAL)
		lista_productos = ListaCaja(sub_paned1, yscrollcommand=y_scroll.set)
		y_scroll["command"] =  lista_productos.yview
		lista_productos.yview_scroll(3,UNITS)
		productos = Controlador.ver_producto_orden()
		if productos:
			for i in productos:
				lista_productos.insert(END, i.__str__())
		else: 
			lista_productos.insert(END, 'Sin articulos')
			boton2.configure(state=DISABLED)
			lista_productos.configure(state=DISABLED)
		lista_productos.pack(side=TOP, fill=BOTH)
		string_total = 'Total: Gs. ' + str(Controlador.total_orden())
		total = Label(sub_paned2, text=string_total, font=("Arial Bold", 15), fg='red', bg='white', borderwidth=0)
		total.pack(side=BOTTOM)
		#se agregan los objetos al paned correspondiente
		sub_paned1.add(lista_productos)
		sub_paned1.add(y_scroll)
		sub_paned2.add(total)
		paned1.add(titulo)
		paned1.add(boton1)
		paned2.add(sub_paned1)
		paned2.add(sub_paned2)
		paned3.add(boton3)
		paned3.add(boton2)
		paned1.pack(side=TOP)
		paned3.pack(side=BOTTOM)
		paned2.pack(side=BOTTOM)
		#callback invocado cuando se hace click sobre un elemento de la lista
		lista_productos.bind('<Button-1>', select_index)
		ventana_top.mainloop()


	@staticmethod
	def menu_tipos_productos(ventana_anterior, ventana):
		"""Metodo para crear un menu de tipos de productos a la venta"""

		def agregado(item, helado=False, *envase):
			"""Metodo que agrega un producto a la orden y muestra un mensaje brevemente"""

			if helado:
				Controlador.agregar_producto_orden(item, helado, *envase)
			else:
				Controlador.agregar_producto_orden(item)
			paned_superior.add(lbl_agregado)
			paned_superior.after(1000, lambda:paned_superior.remove(lbl_agregado))

		def cerrar():
			"""Metodo para cerrar la ventana y volver a la anterior"""

			ventana_top.destroy()
			Vista.menu_vender(ventana)
			

		ventana_anterior.destroy()
		ventana_top = VentanaToplevel(ventana)
		ventana_top.geometry("600x670+400+0")
		cerrar = BotonAtrasCancelar('CERRAR', evento=cerrar, parent=ventana_top)
		cerrar.pack(anchor=S+E)
		paned_superior = Paned(ventana_top)
		lbl_agregado = Label(paned_superior, text='Se agregó el artículo', fg='white') 
		paned_superior.pack(fill=X)
		paned1 = Paned(ventana_top)
		sub_paned1 = Paned(paned1, orient=VERTICAL)
		sub_paned2 = Paned(paned1, orient=VERTICAL)
		sub_paned3 = Paned(paned1, orient=VERTICAL)
		sub_paned4 = Paned(paned1, orient=VERTICAL)
		paned2 = Paned(ventana_top)
		paned3 = Paned(ventana_top)
		paned4 = Paned(ventana_top)
		#se obtiene la lista de helados
		lista_helados = Controlador.lista_helados()
		label_cucurucho = Titulo('Cucurucho', parent=sub_paned1)
		label_cucurucho.configure(font=("Arial", 15))
		sub_paned1.add(label_cucurucho)	
		for envase, helado in lista_helados[:4]:
			boton = Boton(helado.descripcion, evento=lambda envase=envase,helado=helado:agregado(helado, True, envase), parent=sub_paned1)
			sub_paned1.add(boton)
		label_envase = Titulo('Vaso', parent=sub_paned2)
		label_envase.configure(font=("Arial", 15))
		sub_paned2.add(label_envase)	
		for envase, helado in lista_helados[4:8]:
			boton = Boton(helado.descripcion, evento=lambda envase=envase,helado=helado:agregado(helado, True, envase), parent=sub_paned2)
			sub_paned2.add(boton)
		label_isopor = Titulo('Isopor', parent=sub_paned3)
		label_isopor.configure(font=("Arial", 15))
		label_isopor.pack(side=TOP)
		sub_paned3.add(label_isopor)	
		for envase, helado in lista_helados[8:12]:
			boton = Boton(helado.descripcion, evento=lambda envase=envase,helado=helado:agregado(helado, True, envase), parent=sub_paned3)		
			sub_paned3.add(boton)
		label_balde = Titulo('Balde', parent=sub_paned4)
		label_balde.configure(font=("Arial", 15))
		sub_paned4.add(label_balde)
		for envase, helado in lista_helados[12:16]:
			boton = Boton(helado.descripcion, evento=lambda envase=envase,helado=helado:agregado(helado, True, envase), parent=sub_paned4)
			sub_paned4.add(boton)
		for item in Controlador.lista_envases():
			boton = Boton(item.descripcion, evento=lambda item=item:agregado(item), parent=paned2)
			paned2.add(boton)
		for item in Controlador.lista_bebidas():
			boton = Boton(item.descripcion, evento=lambda item=item:agregado(item), parent=paned3)
			paned3.add(boton)
		for item in Controlador.lista_tortas():
			boton = Boton(item.descripcion, evento=lambda item=item:agregado(item), parent=paned4)
			paned4.add(boton)
		titulo_helados = Titulo("Helados por envase", ventana_top)
		paned1.add(sub_paned1)
		sub_paned1.pack(side=LEFT)
		sub_paned2.pack(side=LEFT)
		sub_paned3.pack(side=LEFT)
		sub_paned4.pack(side=LEFT)
		paned1.pack(side=TOP)
		titulo_envases = Titulo("Envases Individuales", ventana_top)
		titulo_envases.pack(side=TOP)
		paned2.pack(side=TOP)
		titulo_bebidas = Titulo("Bebidas", ventana_top)
		titulo_bebidas.pack(side=TOP)
		paned3.pack(side=TOP)
		titulo_tortas = Titulo("Tortas", ventana_top)
		titulo_tortas.pack(side=TOP)
		paned4.pack(side=TOP)
		ventana_top.mainloop()

	@staticmethod
	def confirmar_cliente(ventana_anterior, ventana):
		"""Metodo para seleccionar si el comprobante es con o sin nombre"""

		ventana_anterior.destroy()
		ventana_top = VentanaToplevel(ventana)
		ventana_top.geometry("384x320+500+200")
		titulo = Titulo('Comprobante', ventana_top)
		titulo.pack()
		boton1 = Boton('Con nombre', evento=lambda:Vista.formulario_buscar_cliente(ventana_top, ventana, True), parent=ventana_top)
		boton2 = Boton('Sin nombre', evento=lambda:Vista.imprimir_comprobante(ventana_top, ventana), parent=ventana_top)
		boton1.pack(fill=X)
		boton2.pack(fill=X)
		ventana_top.mainloop()

	@staticmethod
	def imprimir_comprobante(ventana_anterior, ventana, cliente=None):
		"""Metodo para imprimir un comprobante"""

		ventana_anterior.destroy()
		ventana_top = VentanaToplevel(ventana)
		ventana_top.geometry("350x550+500+100")
		titulo = Titulo('Comprobante', ventana_top)
		titulo.pack()
		comprobante = Controlador.obtener_comprobante(cliente).__str__()
		text = Text(ventana_top, bg='white', fg='black')
		text.insert(INSERT, comprobante)
		text.config(state=DISABLED)
		text.pack(side=TOP)
		boton = Boton('TERMINAR', evento=lambda:ventana_top.destroy(), parent=ventana_top)
		boton.pack(side=BOTTOM)
		ventana_top.mainloop()

	@staticmethod
	def menu_devolver(ventana):
		"""Metodo que muestra el menu para devolver productos"""

		def devolver_producto(item):
			msj = Controlador.devolver_producto(item)
			messagebox.showinfo("Modificar cliente", msj)

		ventana_top = VentanaToplevel(ventana)
		ventana_top.geometry("300x350+500+200")
		label = Label(ventana_top, text='Seleccione para devolver:')
		label.configure(font=("Arial", 15), pady=40, padx=10, bg='#e51924', fg='white')
		label.pack(anchor=N+W)
		for item in Controlador.productos_devolvibles():
			boton = Boton(item.descripcion, evento=lambda item=item:devolver_producto(item), parent=ventana_top)
			boton.pack(fill=X)
		boton3 = BotonAtrasCancelar(texto='Atras', evento=lambda:ventana_top.destroy(), parent=ventana_top)
		boton3.pack(side=BOTTOM, fill=X)
		ventana_top.mainloop()


	@staticmethod
	def menu_clientes(ventana):
		"""Metodo que muestra el menu de clientes"""


		ventana_top = VentanaToplevel(ventana)
		ventana_top.geometry("520x580+500+50")
		titulo = Titulo('Clientes', ventana_top)
		titulo.pack(side=TOP)
		lista_clientes = ListaCaja(ventana_top)
		lista_clientes.configure(width=40)
		index = 0
		for i in Controlador.lista_clientes():
			index = index + 1
			lista_clientes.insert(index, '{:<10}{:<18}{:<18}'.format(str(i.ci), i.nombre[:16], i.apellido[:16]))
		lista_clientes.pack()
		boton1 = Boton('AGREGAR', evento=lambda:Vista.formulario_agregar_cliente(ventana_top, ventana), parent=ventana_top)
		boton2 = Boton('MODIFICAR', evento=lambda:Vista.formulario_modificar_cliente(ventana_top, ventana), parent=ventana_top)
		boton3 = Boton('ELIMINAR', evento=lambda:Vista.formulario_eliminar_cliente(ventana_top, ventana), parent=ventana_top)
		boton4 = Boton('BUSCAR', evento=lambda:Vista.formulario_buscar_cliente(ventana_top, ventana), parent=ventana_top)
		boton5 = BotonAtrasCancelar('ATRAS', evento=lambda:ventana_top.destroy(), parent=ventana_top)
		for i in [boton5, boton4, boton3, boton2, boton1]:
			i.configure(width=47)
			i.pack(side=BOTTOM)
		ventana_top.mainloop()

	@staticmethod
	def formulario_agregar_cliente(ventana_anterior, ventana, comprobante=False, cedula=None):
		"""Metodo para mostrar un formulario y agregar un nuevo cliente"""

		def agregar_cliente():
			cliente = Controlador.agregar_cliente(ci.get(), nombre.get(), apellido.get())
			if comprobante:
				if not isinstance(cliente, Cliente):
					messagebox.showerror("Error", cliente)
				else:
					Vista.imprimir_comprobante(ventana_nueva, ventana, cliente)
			else:
				messagebox.showinfo("Agregar cliente", cliente)  

		def ir_atras():
			ventana_nueva.destroy()
			Vista.menu_clientes(ventana)

		ventana_anterior.destroy()
		ventana_nueva = VentanaLblEnt(parent=ventana, titulo='Nuevo Cliente')
		ventana_nueva.geometry("384x400+500+200")
		ci = StringVar(ventana_nueva)
		if comprobante:
			ci.set(cedula)
		nombre = StringVar(ventana_nueva)
		apellido = StringVar(ventana_nueva)
		ventana_nueva.set_lbl_ent('CEDULA', ci)
		ventana_nueva.set_lbl_ent('NOMBRE', nombre)
		ventana_nueva.set_lbl_ent('APELLIDO', apellido)
		boton = Boton('AGREGAR', evento=agregar_cliente, parent=ventana_nueva)
		boton2 = BotonAtrasCancelar('ATRAS', evento=ir_atras, parent=ventana_nueva)
		boton.pack(side=RIGHT)
		if not comprobante:
			boton2.pack(side=LEFT)
		ventana_nueva.mainloop()

	@staticmethod
	def formulario_modificar_cliente(ventana_anterior, ventana):
		"""Metodo para mostrar un formulario y modificar un cliente"""

		def modificar_cliente():
			cliente = Controlador.modificar_cliente(ci.get(), nombre.get(), apellido.get())
			messagebox.showinfo("Modificar cliente", cliente)

		def ir_atras():
			ventana_nueva.destroy()
			Vista.menu_clientes(ventana)

		ventana_anterior.destroy()
		ventana_nueva = VentanaLblEnt(parent=ventana, titulo='Modificar Cliente')
		ventana_nueva.geometry("384x400+500+200")
		ci = StringVar(ventana_nueva)
		nombre = StringVar(ventana_nueva)
		apellido = StringVar(ventana_nueva)
		ventana_nueva.set_lbl_ent('CEDULA', ci)
		ventana_nueva.set_lbl_ent('NOMBRE', nombre)
		ventana_nueva.set_lbl_ent('apellido', apellido)
		boton = Boton('MODIFICAR', evento=modificar_cliente, parent=ventana_nueva)
		boton2 = BotonAtrasCancelar('ATRAS', evento=ir_atras, parent=ventana_nueva)
		boton.pack(side=RIGHT)
		boton2.pack(side=LEFT)
		ventana_nueva.mainloop()

	@staticmethod
	def formulario_eliminar_cliente(ventana_anterior, ventana, ci=None):
		"""Metodo para mostrar un formulario y eliminar un cliente"""

		def eliminar_cliente():
			
			cliente = Controlador.existe_cliente(ci.get())
			if cliente is not None and ci.get():
				msj_eliminar = '¿Desea Eliminar al cliente?' + '\n' + cliente.__str__()
				msj = messagebox.askquestion ('Eliminar cliente', msj_eliminar, icon = 'warning')	
				if msj == 'yes':
					Controlador.eliminar_cliente(ci.get())
					msj_eliminar = 'Se eliminó el cliente' + '\n' + cliente.__str__()
					messagebox.showinfo('Eliminar cliente', msj_eliminar)
				else:
					return
			elif not ci.get():
				messagebox.showerror('Error', 'Ingrese un numero')
			else: 
				messagebox.showerror('Error', 'No existe el cliente')

		def ir_atras():
			ventana_top.destroy()
			Vista.menu_clientes(ventana)
			
		ventana_anterior.destroy()
		ventana_top = VentanaLblEnt(parent=ventana, titulo='Eliminar Cliente')
		ventana_top.geometry("300x300+500+200")	
		ci = StringVar(ventana_top)
		ventana_top.set_lbl_ent('CEDULA', ci)
		boton = Boton('ELIMINAR', evento=eliminar_cliente, parent=ventana_top)
		boton2 = BotonAtrasCancelar('ATRAS', evento=ir_atras, parent=ventana_top)
		boton.pack(side=RIGHT)
		boton2.pack(side=LEFT)
		ventana_top.mainloop()

	@staticmethod
	def formulario_buscar_cliente(ventana_anterior, ventana, comprobante=False):
		"""Metodo para mostrar un formulario y agregar un nuevo cliente"""

		def ir_atras():
			ventana_top.destroy()
			Vista.menu_clientes(ventana)

		def buscar_cliente():
			cliente = Controlador.existe_cliente(ci.get())
			if not comprobante:
				if cliente is None:
					cliente = 'No existe el cliente'
					messagebox.showinfo("Buscar cliente", cliente)
				else:	
					messagebox.showinfo("Buscar cliente", cliente)
			else:
				if cliente is None:
					Vista.formulario_agregar_cliente(ventana_top, ventana, True, ci.get())
				elif not isinstance(cliente, Cliente):
					messagebox.showerror('Error', cliente)
				else:
					Vista.imprimir_comprobante(ventana_top, ventana, cliente)	

		ventana_anterior.destroy()
		ventana_top = VentanaLblEnt(parent=ventana, titulo='Buscar Cliente')
		ventana_top.geometry("300x300+500+200")
		ci = StringVar(ventana_top)
		ventana_top.set_lbl_ent('CEDULA', ci)
		boton = Boton('BUSCAR', evento=buscar_cliente, parent=ventana_top)
		boton2 = BotonAtrasCancelar('ATRAS', evento=ir_atras, parent=ventana_top)
		boton.pack(side=RIGHT)
		if not comprobante:
			boton2.pack(side=LEFT)
		ventana_top.mainloop()


	@staticmethod
	def menu_caja(ventana):
		"""Metodo para mostrar el total en caja"""

		ventana_top = VentanaToplevel(ventana)
		ventana_top.geometry("384x310+500+200")
		titulo = Titulo('Caja', ventana_top)
		titulo.pack(side=TOP, fill=X)
		total = 'El total en caja es: \n Gs. ' + str(Controlador.total_caja())
		lbl = Label(ventana_top, text=total, pady=30, relief="ridge", width=25)
		lbl.configure(bg='#e51924', fg='white', font=("Arial Bold", 15))
		lbl.pack(fill=Y)
		#locale.setlocale(locale.LC_TIME, "ES")
		dia = datetime.now().strftime('%A, %b %-d, %Y')
		#dia = format_date(dia, locale='es')
		hora = datetime.now().strftime('%H:%M')
		lbl = Label(ventana_top, text=dia + '\n' + hora, pady=30)
		lbl.configure(bg='#e51924', fg='white', font=("Arial Bold", 15))
		lbl.pack()
		boton = BotonAtrasCancelar('ATRAS', evento=ventana_top.destroy, parent=ventana_top)
		boton.pack(side=LEFT)
		ventana_top.mainloop()
		

	@staticmethod
	def formulario_cierres_anteriores(ventana):
		"""Metodo para mostrar los cierres de dias anteriores"""

		fechas={'Enero': 1,
				'Febrero': 2,
				'Marzo': 3,
				'Abril': 4,
				'Mayo': 5,
				'Junio': 6,
				'Julio': 7,
				'Agosto': 8,
				'Septiembre': 9,
				'Octubre': 10,
				'Noviembre': 11,
				'Diciembre': 12}

		def ver_cierres():
			try:
				fecha = dia.get() + '-' + str(fechas[mes.get()]) + '-' + anhio.get()
				ventana_top = VentanaToplevel(ventana)
				ventana_top.geometry("350x350+500+200")
				titulo = Titulo('Ordenes: ' + fecha, ventana_top)
				titulo.pack(side=TOP, fill=Y)
				ordenes = Controlador.ver_ordenes_dia(fecha)
				text = Text(ventana_top, bg='white', fg='black')
				orden = ''
				for i in ordenes:
					text.insert(END, i.__str__())
				text.pack()
			except ValueError:
				messagebox.showerror('Error')

		ventana_top = VentanaLblOpt(parent=ventana, titulo='Cierres Anteriores')
		ventana_top.geometry("300x400+500+100")		
		dia = StringVar(ventana_top)
		mes = StringVar(ventana_top)
		anhio = StringVar(ventana_top)
		ventana_top.set_lbl_opt('AÑO', anhio, [i for i in range(2018,2022)])
		ventana_top.set_lbl_opt('MES', mes, list(fechas.keys()))
		ventana_top.set_lbl_opt('DIA', dia, [i for i in range (1,31)])
		boton = Boton('BUSCAR', evento=ver_cierres, parent=ventana_top)
		boton2 = BotonAtrasCancelar('ATRAS', evento=lambda:ventana_top.destroy(), parent=ventana_top)
		boton.pack(side=RIGHT)
		boton2.pack(side=LEFT)
		ventana_top.mainloop()
		

	@staticmethod
	def menu_cerrar(ventana):
		"""Metodo para realizar el cierre de caja"""

		ventana_top = VentanaToplevel(ventana)
		ventana_top.geometry("384x500+500+100")
		titulo = Titulo('Cierre de Caja', ventana_top)
		titulo.pack(side=TOP, fill=X)
		total = 'El total en caja es: \n Gs. ' + str(Controlador.total_caja())
		lbl = Label(ventana_top, text=total, pady=30, relief="ridge", width=25)
		lbl.configure(bg='#e51924', fg='white', font=("Arial Bold", 15))
		lbl.pack(fill=Y)
		dia = datetime.now().strftime('%A, %b %-d, %Y')
		hora = datetime.now().strftime('%H:%M')
		lbl = Label(ventana_top, text=dia + '\n' + hora, pady=30)
		lbl.configure(bg='#e51924', fg='white', font=("Arial Bold", 15))
		lbl.pack()
		boton = BotonAtrasCancelar('Cerrar', evento=lambda:ventana.destroy(), parent=ventana_top)
		boton.pack(side=LEFT, fill=Y)
		fecha = datetime.now().strftime('%-d-%m-%Y')
		ordenes = Controlador.hacer_cierre(fecha)
		text = Text(ventana_top, bg='white', fg='black')
		orden = ''
		for i in ordenes:
			orden = orden + i.__str__() + '\n'	
		text.insert(INSERT, orden)
		text.config(state=DISABLED)
		text.pack()
		scrollY= Scrollbar(ventana_top, orient=VERTICAL,command=text.yview)
		scrollY.pack(side=LEFT)
		text["yscrollcommand"] = scrollY.set
		ventana_top.mainloop()
